module PreprocessorTest(main) where

import Control.Monad.Identity (Identity)
import Control.Monad.Trans.Except (runExceptT)
import Data.Functor.Identity (runIdentity)
import Data.Map (Map)
import qualified Data.Map as Map
import Preprocessor
import Test.Tasty
import Test.Tasty.HUnit

mainFile = ("a", "--#import b\n\
                 \--#import c\n" )

diamondFile = ("b", "--#import c\n\
                    \--#pragma program program\n" )

pragmafile = ( "c", "--#pragma runprogram runProgram\n" )

regularFile = ("fn", "let Nat : U = (A:U) -> A -> (A->A) -> A in Nat")

passingTree :: String -> Identity (Maybe String)
passingTree = return . Just . get
    where get = (Map.!)
              . Map.fromList
              $ [ mainFile, diamondFile, pragmafile, regularFile ]

treeAssert = testCase "Correct dependency ordering" $ do
    assertBool "Dependency tree is correct" (case result of Right _ -> True; _ -> False)
    case result of
        Right (files,pragmas) ->
            assertEqual "Dependency order is correct" ["c","b","a"] files
        Left _ -> assertBool "Dependency order is correct" False
    where result = runIdentity . runExceptT $ preprocessor passingTree "a"

regularFileTest =
    testCase "Single file without imports or pragmas" .
    assertEqual "" (Right (["fn"],[])) .
    runIdentity . runExceptT .
    preprocessor passingTree $
    "fn"

rightTests =
    testGroup "Passing inputs" [treeAssert, regularFileTest]

main = defaultMain rightTests
