{-# language OverloadedStrings #-}
module SyntaxTest where
{-
import Data.String
import Syntax
import Test.Tasty
import Test.Tasty.HUnit

main :: IO ()
main = defaultMain tests

($$) :: Tm -> Tm -> Tm
($$) = Apply
infixl 7 $$

(==>) a b = Pi "_" a b
infixr 4 ==>

(=:>) (x,a) b = Pi x a b
infixr 4 =:> 

instance IsString Tm where
  fromString = Var

piTest = ( "(x:A) -> A2"
         , ("x","A") =:> "A2" )

letTest = ( "let id : (A:U) -> (x:A) -> A = \\t.\\x.x in id"
          , Let "id" (("A",U) =:> ("x","A") =:> "A") 
                (Lambda "t" $ Lambda "x" "x")
            "id"
          )

piShort = ( "A -> B -> C"
          , "A" ==> "B" ==> "C" )

tests =
    testGroup "Right Syntax" .
    map (\(src,tm) -> testCase "" .
                      assertEqual src (Right tm) .
                      parseTT "source" $ src) $
    [ piTest
    , letTest
    , piShort ]

funcTest = parseTT "" "let Func : U = (A:U) -> (B:U) -> (x:A) -> B in Func" -}