{-# language LambdaCase #-}

module Preprocessor( Pragma(..)
                   , PragmaDef
                   , PreError(..)
                   , Filename
                   , preprocessor
                   ) where

--import Control.DeepSeq (force)
import Control.Monad (mfilter,foldM,forM)
import Control.Applicative ((<|>))
import Control.Monad.Identity (Identity)
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Except (ExceptT,runExceptT,throwE)
import Control.Monad.Trans.Reader (ReaderT,ask,runReaderT)
import Data.Foldable (foldrM)
import Data.Graph (Graph,Vertex)
import qualified Data.Graph as Graph
import Data.Map (Map)
import Data.Map.Strict ((!?))
import qualified Data.Map.Strict as Map
import Data.Maybe (maybe, mapMaybe)
import Data.Set (Set)
import qualified Data.Set as Set
import GHC.Exts (groupWith)
import Pragmas(Pragma,parsePragma)

type Filename = String

data PreError
    = SelfInclude Filename
    | UnknowDirective Filename String
    | UnknownPragma Filename String
    | CantOpenFile Filename
    | PragmaDefined Filename String
    | CyclicDependency Filename [Filename]
    deriving (Eq)

instance Show PreError where
    show = \case
        SelfInclude filename -> "file " ++ show filename ++ "imports itself"
        UnknowDirective fn directive -> show fn ++ ": unkown directive " ++ show directive
        UnknownPragma fn pragma -> show fn ++ ": unkown pragma " ++ show pragma
        CantOpenFile filename -> "can't open file " ++ show filename
        PragmaDefined fn pragma -> "pragma " ++ show pragma ++ " already declared in " ++ show fn
        CyclicDependency fn loop -> "cyclic dependency in " ++ show fn ++ " through " ++ show loop

type PreM = Either PreError

type PreFileM = ReaderT Filename PreM

type PragmaDef = (Pragma,String)

data Include = Include { source :: Filename, request :: Filename}

data Directive
    = DirPragmaDef PragmaDef
    | DirInclude Include

preError = Left

includes :: [Directive] -> [Include]
includes = mapMaybe (\der -> case der of
                        DirInclude inc -> Just inc
                        _ -> Nothing
                    )

pragmas :: [Directive] -> [PragmaDef]
pragmas = mapMaybe (\der -> case der of
                        DirPragmaDef def -> Just def
                        _ -> Nothing
                    )

preLines :: String -> [String]
preLines = takeWhile isComment . mapMaybe (mfilter ("" /=) . Just) . lines
    where isComment str = case str of
                            ('-':'-':'#':_) -> True
                            _ -> False

processLine :: String -> PreFileM Directive
processLine ('-':'-':'#':line) = do
    source <- ask
    case words line of
        ["import",filename] ->
            return $ DirInclude $ Include source filename
        ["pragma",pragma,binding] ->
            lift
            . maybe
                (preError $ UnknownPragma source pragma)
                (\pragma -> return $ DirPragmaDef (pragma,binding))
            . parsePragma
            $ pragma
        _ -> lift . preError $ UnknowDirective source line

insert :: [Directive] -> Directive -> PreFileM [Directive]
insert dirs dir = do
    filename <- ask
    case dir of
        DirInclude (Include src req) ->
            if src == req then
                lift $ preError $ SelfInclude src
            else
                return (dir:dirs)
        DirPragmaDef (pragma,_) ->
            lift
            . maybe
                (return $ dir:dirs)
                (preError . PragmaDefined filename)
            . lookup pragma
            . pragmas
            $ dirs

alreadyProcessed :: [Directive] -> Set Filename
alreadyProcessed =
    Set.fromList
    . mapMaybe (\dir -> case dir of
                            DirInclude (Include src _) -> Just src
                            _ -> Nothing
                )

directives :: String -> PreFileM [Directive]
directives =
    (>>= foldM insert []) . mapM processLine . preLines

joinFiles :: [Directive] -> [Directive] -> PreFileM [Directive]
joinFiles = foldrM $ flip insert 

toExcept :: (Monad m) => Either a b -> ExceptT a m b
toExcept = either throwE return

data BreadthState = BreadthState { vertQueue :: [Filename]
                                 , visited :: Set Filename
                                 , innerState :: [Directive] }

start :: Filename -> BreadthState
start fn = BreadthState [fn] Set.empty []

newEdges :: BreadthState -> [Include] -> [Filename]
newEdges (BreadthState xs visited _) follow = newIncludes
    where
        notGrey :: Filename -> Bool
        notGrey = flip notElem xs
        newIncludes :: [Filename]
        newIncludes = filter notGrey . filter (not . flip Set.member visited) . map request $ follow

stepBreadth :: BreadthState -> [Directive] -> PreM BreadthState
stepBreadth bs@(BreadthState (x:xs) visited state) follow = do
    let edges' = newEdges bs (includes follow)
    state' <- runReaderT (joinFiles follow state) x
    let filename = x
    return $ BreadthState (xs ++ edges') (Set.insert filename visited) state'

breadthLoop :: (Monad m) => BreadthState -> ReaderT (Filename -> m (Maybe String)) (ExceptT PreError m) [Directive]
breadthLoop (BreadthState [] _ state) = return state
breadthLoop bs@(BreadthState (filename:_) _ _) = do
    readFile <- ask
    content <- lift . convertRead readFile $ filename
    dirs <- lift . toExcept . runReaderT (directives content) $ filename
    dirs `seq` return ()
    bs' <- lift . toExcept $ stepBreadth bs dirs
    breadthLoop bs'
    where convertRead :: (Monad m) => (Filename -> m (Maybe String)) -> Filename -> ExceptT PreError m String
          convertRead readMaybe filename = do
              readResult <- lift . readMaybe $ filename
              case readResult of
                  Nothing -> throwE . CantOpenFile $ filename
                  Just content -> return content

dependencyGraph :: Filename -> [Include] -> [(Filename, Filename, [Filename])]
dependencyGraph start =
    map (\(src,trgs) -> (src,src,Set.toList trgs))
    . Map.toList
    . foldl (\edges (Include src trg) -> insertOutEdge trg
                                       . Map.insertWith Set.union src (Set.singleton trg)
                                       $ edges
            )
            (Map.singleton start Set.empty)
    where insertOutEdge :: (Ord a) => a -> Map a (Set a) -> Map a (Set a)
          insertOutEdge edge map = case map !? edge of
                                        Just _ -> map
                                        Nothing -> Map.insert edge Set.empty map

preprocessor :: (Monad m) => (Filename -> m (Maybe String)) -> Filename -> ExceptT PreError m ([Filename],[PragmaDef])
preprocessor read filename = do
    directives <- runReaderT (breadthLoop . start $ filename) read
    let edges = dependencyGraph filename . includes $ directives
    let scc = Graph.stronglyConnComp edges
    let cycle = foldr (\scc ->
                        (<|>) ( case scc of Graph.CyclicSCC cyc -> Just cyc
                                            _ -> Nothing )
                      ) Nothing scc
    toExcept $
        maybe (return ())
              (preError . CyclicDependency filename)
              cycle
    let (graph,get,_) = Graph.graphFromEdges edges
    let includeOrder = map ((\(x,_,_) -> x) . get) . Graph.topSort $ graph
    return (reverse includeOrder, pragmas directives)
