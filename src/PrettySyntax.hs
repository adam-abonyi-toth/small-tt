module PrettySyntax(tmDoc,renderTm,errDoc,renderErr,help) where

import Syntax
import TypeCheck
import Pragmas(Pragma)
import Prelude hiding (print)
import Text.PrettyPrint

isAtom :: Tm -> Bool
isAtom Var{} = True
isAtom U{} = True
isAtom Nat{} = True
isAtom Zero{} = True
--isAtom Export = True
isAtom _ = False

atomic :: Tm -> Doc
atomic tm = (if isAtom tm then id else parens) (tmDoc tm)

tmDoc :: Tm -> Doc
tmDoc (Var _ name _) = text name
tmDoc Nat{} = text "Nat"
tmDoc Zero{} = text "zero"
tmDoc (Suc _ t _) = text "suc" <+> atomic t
tmDoc tm@(Apply f u) = doc
    where applyChain (Apply f u) = case f of
              Apply{} -> u : applyChain f
              _ -> [u,f]
          doc = case map atomic' . reverse $ applyChain tm of
              (f:xs) -> f $$ nest 4 (foldl ($+$) empty xs)
              [] -> empty
          atomic' tm = if isAtom tm then tmDoc tm else parens (tmDoc tm)
tmDoc tm@Lambda{} =
    case getNamesTerm tm of
        (bindings,body) -> bindings $$ nest 4 (tmDoc body)
    where binding name = text "\\" <> text name <> char '.'
          getNamesTerm tm = case tm of
            Lambda _ name tm _ ->
                let (rest,tm') = getNamesTerm tm in
                (binding name <+> rest,tm')
            _ -> (empty,tm)
tmDoc (Pi _ x ty tm _) = binding <+> text "->" $+$ tmDoc tm
    where binding = case ty of
              Pi{} -> lparen
                      $$ nest 2 (if x == "_" then tmDoc ty else text x <> char ':' <> tmDoc ty)
                      <+> rparen
              _ -> if x == "_" then tmDoc ty else parens (text x <> char ':' <> tmDoc ty)
tmDoc (IndNat _ p z s n _) = text "indNat" $$ nest 7 (atomic p $+$ atomic z $+$ atomic s $+$ atomic n)
tmDoc (Let _ x ty tm tm') = hsep [ text "let"
                                 , text x
                                 , char ':'
                                 , tmDoc ty
                                 , char '=' ]
                            $+$ nest 4 (tmDoc tm)
                            $+$ text "in"
                            $+$ tmDoc tm'
tmDoc  U{} = char 'U'
--tmDoc Export = text "#export"

renderTm :: Tm -> String
renderTm = render . tmDoc

instance Show Tm where
    show = renderTm

print :: Show a => a -> Doc
print = text . show

errDoc :: Error -> Doc
errDoc (VarNotFound ss name) = text "variable not found: " <> text name
                               <+> (text . show $ ss)
errDoc (TypeMismatch ss ty ty') =
    text "type mismatch:" <+> (text . show $ ss) $+$
    nest 4 (text "Expected type: " $+$ nest 4 (tmDoc ty)) $+$
    nest 4 (text "Actual type: " $+$ nest 4 (tmDoc ty'))
errDoc (AppliedNotAFunction ss) = text "applied is not a function:"  <+> (text . show $ ss)
errDoc (TypeExpected ss) = text "type expected:"  <+> (text . show $ ss)
errDoc (CantInfer ss) = text "can't infer type:" <+> (text . show $ ss)
errDoc (MissingPragma pragma) = text "missing declaration for pragma '" <> print pragma <> text "'"
errDoc (MissingPragmaDef pragma) = text "missing definition for pragma '" <> print pragma <> text "'"
errDoc (MissingPragmaTy pragma) = text "missing type for pragma '" <> print pragma <> text "'"

renderErr :: Error -> String
renderErr = render . errDoc

instance Show Error where
    show = renderErr

help :: String
help = renderStyle (style { lineLength = 30 }) fullText
    where
        commandData :: [(String,String)]
        commandData = [ ("--help","Display this message")
                      , ("--version","Display version information")
                      , ("-nf, --normalform","Compute and show the normal form of the loaded expression. A file must be specified.")
                      , ("--run", "Run the interactive program. Overrides -nf. A file must be specified.")]
        docCommand (command,description) =
            text command $+$ nest 4 (text description)
        dataDoc = vcat . map docCommand $ commandData
        fullText = text "usage: small-tt-exe filename [parameters]"
                 $$ text "available parameters:"
                 $$ nest 4 dataDoc

