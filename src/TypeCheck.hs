module TypeCheck(CheckM,Error(..),SemanticConfig(..),semantics) where

import qualified Syntax
import Pragmas (Pragma(..))
import Preprocessor (PragmaDef)
import Control.Monad
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.State.Lazy (StateT,get,put,gets,modify,runStateT,evalStateT)
import Data.Either (either)
import Data.List (lookup)
import Data.Maybe (fromMaybe)
import Data.Text.Lazy (unpack)
import Debug.Trace(traceM)
import System.IO (hFlush,stdout)
import Text.Parsec.Pos (SourcePos,initialPos,newPos,sourceName,sourceLine,sourceColumn)
import Text.Pretty.Simple (pShow)
import Text.Read (readMaybe)

type Name = String

data SourceSpan = SourceSpan { srcFrom :: SourcePos, srcTo :: SourcePos }

type SP = SourcePos
type SS = SourceSpan

mkSS = SourceSpan

pos0 = initialPos ""

ss0 = SourceSpan pos0 pos0

instance Show SourceSpan where
    show (SourceSpan from to) =
        if sourceName from == sourceName to then
            show (sourceName from) ++ " " ++ showPosCoord from ++ "-" ++ showPosCoord to
        else
             show from ++ " - " ++ show to
        where showPosCoord sPos =
                  "(line " ++ show (sourceLine sPos) ++ ", column " ++ show (sourceColumn sPos) ++ ")"
                                            

data Tm
    = Var SS Name
    | Gen Int
    | Apply Tm Tm
    | Lambda SS Name Tm
    | Pi SS Name Tm Tm
    | Let SP Name Tm Tm Tm
    | Nat SS
    | Zero SS
    | Suc SS Tm
    | IndNat SS Tm Tm Tm Tm
    | U SS
    | Trap Int [Tm]
    deriving (Show)

($$) :: Tm -> Tm -> Tm
($$) = Apply
infixl 7 $$

(@@) :: Val -> Val -> Val
f @@ x = case (f, x) of
    (VLambda _ f,    x) -> f x
    (VTrap n params, x) -> VTrap n (x:params)
    (_,              _) -> VApply f x
infixl 7 @@

(==>) = Pi ss0 "_"
infixr 4 ==>

(=:>) (x,a) = Pi ss0 x a
infixr 4 =:>

lambda = Lambda ss0
var = Var ss0
nat = Nat ss0
suc = Suc ss0
u = U ss0
zero = Zero ss0

printVals :: Vals -> String
printVals = unpack . pShow . map (\(a,b) -> (a,quote' <$> b))

quote :: Int -> Val -> Syntax.Tm
quote n val = case val of
    VVar name -> Syntax.Var pos0 name pos0
    VGen i -> Syntax.Var pos0 ("$" ++ show i) pos0
    VApply v1 v2 -> Syntax.Apply (quote n v1) (quote n v2)
    VLambda name f -> Syntax.Lambda pos0 name (quote (n+1) . f . VVar $ name) pos0
    VPi name ty f -> Syntax.Pi pos0 name (quote n ty) (quote (n+1) . f . VVar $ name) pos0
    VU -> Syntax.U pos0 pos0
    VNat -> Syntax.Nat pos0 pos0
    VZero -> Syntax.Zero pos0 pos0
    VSuc v -> Syntax.Suc pos0 (quote n v) pos0
    VIndNat p z s ntm -> Syntax.IndNat pos0 (quote n p) (quote n z) (quote n s) (quote n ntm) pos0

quote' :: Val -> Tm
quote' val = case val of
    VVar name -> Var ss0 name
    VGen i -> Var ss0 ("$" ++ show i)
    VApply v1 v2 -> quote' v1 $$ quote' v2
    VLambda name f -> Lambda ss0 name (quote' . f . VVar $ name)
    VPi name ty f -> Pi ss0 name (quote' ty) (quote' . f . VVar $ name)
    VU -> U ss0
    VZero -> Zero ss0
    VNat -> Nat ss0
    VSuc v -> Suc ss0 $ quote' v
    VIndNat p z s n -> IndNat ss0 (quote' p) (quote' z) (quote' s) (quote' n)
    VTrap n vals -> Trap n (map quote' vals)

unquote :: Syntax.Tm -> Tm
unquote = uq where
    uq (Syntax.Var p0 name p1) = Var (mkSS p0 p1) name
    uq (Syntax.Apply t t') = Apply (uq t) (uq t')
    uq (Syntax.Lambda p0 x t p1) = Lambda (mkSS p0 p1) x (uq t)
    uq (Syntax.Pi p0 name t t' p1) = Pi (mkSS p0 p1) name (uq t) (uq t')
    uq (Syntax.Let pos x ty def t) = Let pos x (uq ty) (uq def) (uq t)
    uq (Syntax.IndNat p0 p z s n p1) = IndNat (mkSS p0 p1) (uq p) (uq z) (uq s) (uq n)
    uq (Syntax.Suc p0 tm p1) = Suc (mkSS p0 p1) (uq tm)
    uq (Syntax.Zero p0 p1)  = Zero (mkSS p0 p1)
    uq (Syntax.Nat p0 p1) = Nat (mkSS p0 p1)
    uq (Syntax.U p0 p1) = U (mkSS p0 p1)

getSourceSpan :: Tm -> SourceSpan
getSourceSpan tm = case tm of
    U ss -> ss
    Zero ss -> ss
    Suc ss _ -> ss
    Nat ss -> ss
    Apply t u -> SourceSpan (srcFrom (getSourceSpan t)) (srcTo (getSourceSpan u))
    Lambda ss _ _ -> ss
    Pi ss _ _ _ -> ss
    Let pos _ _ def _ -> SourceSpan pos (srcTo (getSourceSpan def))
    Var ss name -> ss
    IndNat ss p z s n -> ss
    Gen i -> ss0
    Trap i xs -> ss0

data Val
    = VVar Name
    | VGen Int
    | VApply Val Val
    | VLambda Name (Val -> Val)
    | VPi Name Val (Val -> Val)
    | VNat
    | VZero
    | VSuc Val
    | VIndNat Val Val Val Val
    | VU
    | VTrap Int [Val]

type Vals = [(Name, Maybe Val)]

-- Assumption: Tm well-typed
-- Alternatíva: type-directed eval
eval :: Vals -> Tm -> Val
eval env t = case t of
    U _ -> VU
    Zero _ -> VZero
    Suc _ t -> VSuc (eval env t)
    Nat _ -> VNat
    Apply t u -> eval env t @@ eval env u
    Lambda _ x t -> VLambda x (\v -> eval ((x,Just v):env) t)
    Pi _ x t t' -> VPi x (eval env t) (\v -> eval ((x,Just v):env) t')
    Let _ x _ def t -> eval ((x,Just $ eval env def):env) t
    Var _ name -> fromMaybe (VVar name) .
                fromMaybe (error $ "impossible because t is well-typed (btw "++show name++")\n"++printVals env) .
                lookup name $ env
    IndNat _ p z s n -> ind nv
        where nv = eval env n
              zv = eval env z
              sv = eval env s
              ind :: Val -> Val
              ind VZero = zv
              ind (VSuc vtm) = sv @@ vtm @@ ind vtm
              ind vtm = VIndNat (eval env p) (eval env z) (eval env s) vtm
    Gen i -> VGen i
    Trap i xs -> VTrap i $ map (eval env) xs

hostInt :: Val -> Int
hostInt = hostInt' 0
    where hostInt' n VZero = n
          hostInt' n (VSuc v) = let n' = n+1 in seq n' (hostInt' n' v)
          hostInt' _ _ = -100000000

targetInt :: Int -> Val
targetInt 0 = VZero
targetInt n | n > 0 = VSuc (targetInt (n-1))
targetInt _ = VZero

type Ty = Tm
type VTy = Val
type VTm = Val
type VTys = [(Name,VTy)]

data Con = Con { conSize :: Int, conTys :: VTys, conVals :: Vals}

gen :: Con -> VTm
gen (Con i _ _) = VGen i

define :: Con -> Name -> VTy -> VTm -> Con
define (Con i tys vals) name ty tm =  Con i ((name,ty):tys) ((name,Just tm):vals)

bind :: Con -> Name -> VTy -> Con
bind (Con i tys vals) name ty = Con (i+1) ((name,ty):tys) ((name,Nothing):vals)

conv :: Int -> Val -> Val -> Bool
conv = go where
    go i VU                 VU                   = True
    go i VNat               VNat                 = True
    go i VZero              VZero                = True
    go i (VVar x)          (VVar x')             = x == x'
    go i (VGen j)          (VGen k)              = j == k
    go i (VApply t u)      (VApply t' u')        = go i t t' && go i u u'
    go i (VLambda _ f)     (VLambda _ f')        = go (i+1) (f $ VGen i) (f' $ VGen i)
    go i (VPi _ ty p)      (VPi _ ty' p')        = go i ty ty' && go (i+1) (p $ VGen i) (p' $ VGen i)
    go i (VSuc v)         (VSuc v')              = go i v v'
    go i (VIndNat p z s n) (VIndNat p' z' s' n') = go i p p' && go i z z' && go i s s' && go i n n'
    go i _                 _                     = False

data Error
    = VarNotFound SS Name
    | TypeMismatch SS Syntax.Tm Syntax.Tm
    | AppliedNotAFunction SS
    | TypeExpected SS
    | CantInfer SS
    | MissingPragma Pragma
    | MissingPragmaDef Pragma
    | MissingPragmaTy Pragma

type CheckM = StateT Con (Either Error)

typeError :: Error -> CheckM a
typeError err = lift (Left err)

bound :: Name -> VTy -> CheckM a -> CheckM a
bound x ty comp = do
    con <- get
    put (bind con x ty)
    result <- comp
    put con
    return result

check :: Tm -> VTy -> CheckM ()
check tm vty = case (tm, vty) of
    (Lambda _ x t, VPi _ ty ty') ->
        bound x ty $ check t (ty' (VVar x))
    (tm, vty) -> do
        vty' <- infer tm
        con <- gets conSize
        unless (conv con vty vty')
            $ typeError (TypeMismatch (getSourceSpan tm) (quote 0 vty) (quote 0 vty'))

infer :: Tm -> CheckM VTy
infer tm = case tm of
    Var ss name -> do
        con <- get
        maybe (typeError $ VarNotFound ss name) return . lookup name . conTys $ con
    U _ -> return VU
    Nat _ -> return VU
    Zero _ -> return VNat
    Suc _ tm -> do
        check tm VNat
        return VNat
    Pi _ x ty ty' -> do
        check ty VU
        vals <- gets conVals
        let vty = eval vals ty
        bound x vty $ check ty' VU
        return VU
    Apply f x -> do
        fty <- infer f
        case fty of
            VPi _ ty ty' -> do
                check x ty
                vals <- gets conVals
                return $ ty' (eval vals x)
            _ ->
                typeError $ AppliedNotAFunction (getSourceSpan f)
    Let _ name ty def t -> do
        tty <- infer ty
        case tty of
            VU -> do
                vals <- gets conVals
                let ty' = eval vals ty
                check def ty'
                modify (\con -> define con
                                       name
                                       ty'
                                       (eval vals def))
                infer t
            _ -> typeError $ TypeExpected (getSourceSpan ty)
    IndNat _ p z s n -> do
        vals <- gets conVals
        let pTy = eval vals $ nat ==> u
        let sucTy = eval vals $ ("$k",nat) =:> (p $$ var "$k") ==> (p $$ suc (var "$k"))
        check p pTy
        check z $ eval vals (p $$ zero)
        check s sucTy
        check n VNat
        return $ eval vals (p $$ n)
    _ -> typeError $ CantInfer (getSourceSpan tm)

con0 = Con 0 [] []
{-
infer0 :: Tm -> Either Error Syntax.Tm
infer0 t = quote 0 <$> evalStateT (infer t) con0

nf0 :: Syntax.Tm -> CheckM Syntax.Tm
nf0 t = do
    infer con0 (unquote t)
    return $ quote 0 . eval [] . unquote $ t
-}
data SemanticConfig = SemanticConfig { run :: Bool, pragmas :: [PragmaDef]  }

semantics :: SemanticConfig -> Syntax.Tm -> Either Error (Either Syntax.Tm (IO Int))
semantics config preTm = do
    let term = unquote preTm
    (ty,con) <- runStateT (infer term) con0
    if run config then do
        programData <- tryRun term ty con (pragmas config)
        return (Right (runProgram programData))
    else
        return . Left . quote 0 . eval [] $ term
        
data ProgramData = ProgramData { programBody :: Val, programSemantics :: Val }

tryRun :: Tm -> VTy -> Con -> [PragmaDef] ->  Either Error ProgramData
tryRun tm vty con pragmas = do
    (progDef,progTy) <- getPragmaDefTy Program
    (runDef,runTy) <- getPragmaDefTy RunProgram
    checkEq programType progTy
    checkEq (runProgramType progDef) runTy 
    checkEq (progDef @@ VNat) vty
    return (ProgramData (eval [] tm) (eval [] . quote' $ runDef @@ VNat))
    where requirePragma :: Pragma -> Either Error String
          requirePragma pragma = maybe (Left . MissingPragma $ pragma) return . flip lookup pragmas $ pragma
          vals = conVals con
          tys = conTys con
          getPragmaDefTy :: Pragma -> Either Error (Val,VTy)
          getPragmaDefTy pragma = do
              name <- requirePragma pragma
              let errorDef = Left . MissingPragmaDef $ pragma
              def <- maybe errorDef (maybe errorDef return) $ lookup name vals
              let errorTy = Left . MissingPragmaTy $ pragma
              ty <- maybe errorTy return . lookup name $ tys
              return (def,ty)
          programType :: Val
          programType = eval [] $ u ==> u
          natProgram :: Val
          natProgram = programType @@ VNat
          runProgramType :: Val -> Val
          runProgramType programType =
              withProgramParameter @@ programType
              where either3 :: Tm
                    either3 = lambda "A" $ lambda "B" $ lambda "C" $
                                ("X",u) =:>
                                (var "A" ==> var "X") ==>
                                (var "B" ==> var "X") ==>
                                (var "C" ==> var "X") ==>
                                var "X"
                    tuple :: Tm
                    tuple = lambda "A" $ lambda "B" $ ("X",u) =:> (var "A" ==> var "B" ==> var "X") ==> var "X"
                    withProgramParameter :: Val
                    withProgramParameter = 
                        eval [] $ lambda "$program" $ ("T",u) =:>
                                                      var "$program" $$ var "T" ==>
                                                      (either3 $$ var "T"
                                                               $$ (tuple $$ nat
                                                                         $$ (var "$program" $$ var "T") )
                                                                         $$ (nat ==> (var "$program" $$ var "T")))
          checkEq :: VTy -> VTy -> Either Error ()
          checkEq exp act = unless (conv 0 exp act) (Left $ TypeMismatch sourceSpan (quote 0 exp) (quote 0 act))
              where sourceName = "#pragma checking#"
                    spos = newPos sourceName 0 0
                    sourceSpan = mkSS spos spos

data HostResult
    = Return Int
    | Output (Int,Val)
    | Input (Int -> Val)

runProgram :: ProgramData -> IO Int
runProgram (ProgramData program runProg) =
    runHostResult hostResult
    where result :: Val
          result = runProg @@ program
          hostResult :: HostResult
          hostResult =
            case result @@ VGen (-1) @@ VTrap 0 [] @@ VTrap 1 [] @@ VTrap 2 [] of
                VTrap 0 [t] -> Return (hostInt t)
                VTrap 1 [outProgram] ->
                    case outProgram @@ VGen (-1) @@ VTrap (-1) [] of
                        VTrap (-1) [cont,out] -> Output (hostInt out,cont)
                        _ -> error "impossible"
                VTrap 2 [input] -> Input (\n -> input @@ targetInt n)
                other -> error $ "impossible\n" ++ (show . quote' $ other)
          runHostResult :: HostResult -> IO Int
          runHostResult result = case result of
              Return n -> return n
              Output (n,cont) -> do
                print n
                runProgram (ProgramData cont runProg)
              Input input -> do
                  userInput <- promptInt
                  runProgram (ProgramData (input userInput) runProg)
                  where promptInt :: IO Int
                        promptInt = do
                            putStr "> "
                            hFlush stdout
                            input <- getLine
                            case readMaybe input of
                                Just n | n >= 0 -> return n
                                _ -> do
                                    putStrLn "runtime: not a natural number! (0, 1, ...)"
                                    promptInt
