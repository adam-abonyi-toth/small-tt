{-# language RecordWildCards #-}

module Syntax(Tm(Var,Apply,Lambda,Pi,Let,U,Nat,Zero,Suc,IndNat),LoadType(..),parseTT,loadFiles,SyntaxError) where

import Control.Applicative hiding (many)
import Text.Parsec hiding ((<|>))
import qualified Text.Parsec.Pos (SourcePos)
import qualified Text.Parsec.Token as T
import Data.Functor.Identity

type Parser = Parsec String ()

type SP = Text.Parsec.Pos.SourcePos

type SyntaxError = ParseError

lang :: T.GenLanguageDef String () Identity
lang = T.LanguageDef "{-" "-}" "--" False letter alphaNum empty empty ["let","in","U","Nat","zero","suc","indNat","#export"] [] True

T.TokenParser{..} = T.makeTokenParser lang

type Name = String

data Tm
    = Var SP Name SP
    | Apply Tm Tm
    | Lambda SP Name Tm SP
    | Pi SP Name Tm Tm SP
    | Let SP Name Tm Tm Tm
    | U SP SP
    | DependentBinding Name Tm
    | Nat SP SP
    | IndNat SP Tm Tm Tm Tm SP
    | Zero SP SP
    | Suc SP Tm SP
    | Export
    deriving (Eq)

data LoadType
    = Library (Tm -> Tm)
    | Program Tm

pVar :: Parser Tm
pVar = Var <$> getPosition <*> identifier <*> getPosition

binding :: Parser String
binding = identifier <|> do throw <- many1 (char '_')
                            rest <- many alphaNum
                            return (throw ++ rest)

pU :: Parser Tm
pU = U <$> getPosition <*> (reserved "U" *> getPosition)

pZero :: Parser Tm
pZero = Zero <$> getPosition  <*> (reserved "zero" *> getPosition)

pSuc :: Parser Tm
pSuc = Suc <$> getPosition <*> (reserved "suc" *> pAtom) <*> getPosition

pNat :: Parser Tm
pNat = Nat <$> getPosition <*> (reserved "Nat" *> getPosition)

pLambda :: Parser Tm
pLambda = Lambda <$> (getPosition <* symbol "\\")
                 <*> binding
                 <*> (symbol "." *> pTerm)
                 <*> getPosition

shortPi :: Parser Tm
shortPi = do startPos <- getPosition
             binding <- try depBinding <|> pApply
             arrow <- optionMaybe . try . symbol $ "->"
             case arrow of
                 Nothing -> case binding of
                                DependentBinding _ _ -> unexpected "dependent binding"
                                _ -> return binding
                 _       -> do rest <- pTerm
                               endPos <- getPosition
                               return $ uncurry (Pi startPos) (toBinding binding) rest endPos
       where 
            depBinding :: Parser Tm
            depBinding = parens (DependentBinding <$> binding <*> (symbol ":" *> pTerm))
            toBinding tm =
                case tm of
                    DependentBinding x t -> (x,t)
                    _ -> ("_",tm)

pLet :: Parser Tm
pLet 
    = Let
    <$> (getPosition <*  reserved "let")
    <*> identifier
    <*> (symbol ":" *> pTerm)
    <*> (symbol "=" *> pTerm)
    <*> (reserved "in" *> ((reserved "#export" *> pure Export) <|> pTerm))


pIndNat :: Parser Tm
pIndNat = IndNat 
        <$> (reserved "indNat" *> getPosition)
        <*> pAtom
        <*> pAtom
        <*> pAtom
        <*> pAtom
        <*> getPosition

pApply :: Parser Tm
pApply = chainl1 pAtom (pure Apply)  

pTerm :: Parser Tm
pTerm = shortPi

pAtom :: Parser Tm
pAtom =  pU <|> pIndNat <|> pNat <|> pZero <|> pSuc <|> pVar <|> pLet <|> pLambda <|> parens pTerm

reLetExport :: Tm -> Maybe (Tm -> Tm)
reLetExport (Let pos x t t' t'') = case t'' of Export -> Just thisPart
                                               _      -> do rest <- reLetExport t''
                                                            return (thisPart . rest)
                                where thisPart = Let pos x t t'
reLetExport _                = Nothing

pEntity :: Parser Tm
pEntity = do whiteSpace
             term <- pTerm
             eof
             return term

pLibrary :: Parser (Tm -> Tm)
pLibrary = do term <- pEntity
              case reLetExport term of
                Just tm -> return tm
                _ -> unexpected "top level expression in import, should be \"#export\""

pFile :: Parser LoadType
pFile = do term <- pEntity
           case reLetExport term of
               Just lib -> return (Library lib)
               Nothing -> return (Program term)

load :: [(SourceName,String)] -> Either ParseError LoadType
load [(sn,source)] = parse pFile sn source
load ((sn,src):xs) = do curr <- parse pLibrary sn src
                        rest <- load xs
                        case rest of
                            Library lib -> return . Library $ (curr . lib)
                            Program p -> return . Program . curr $ p

loadFiles :: Monad m => (String -> m String) -> [SourceName] -> m (Either ParseError LoadType)
loadFiles readFile files = load <$> mapM loadFile files
    where
        loadFile fn = do f <- readFile fn
                         return (fn,f)

parseTT :: SourceName -> String -> Either ParseError Tm
parseTT = parse pTerm
