{-# language ScopedTypeVariables #-}

module Compiler(load,run,main,ParameterError(..),CompileError(..)) where

--import Prelude hiding (readFile)
import qualified PrettySyntax
import qualified Preprocessor
import qualified Syntax
import qualified TypeCheck
import Control.Exception (catch)
import Control.Monad (when)
import Control.Monad.Trans.Except (ExceptT,runExceptT)
import Data.Set (Set)
import qualified Data.Set as Set
import System.Environment (getArgs)
import System.Exit (ExitCode(ExitFailure),exitWith,exitSuccess,exitFailure)
import System.IO
import System.IO.Error (IOError)
import Text.Parsec.Pos (newPos)

readFile' :: String -> IO String
readFile' fn = do
    inFile <- openFile fn ReadMode
    contents <- hGetContents inFile
    forceList contents `seq` hClose inFile
    return contents
    where forceList [] = ()
          forceList (x:xs) = forceList xs

safeReadFile' :: String -> IO (Maybe String)
safeReadFile' fn =
    catch (Just <$> readFile' fn)
          (\(_ :: IOError) -> return Nothing)

data CompileError
    = PreError Preprocessor.PreError
    | SyntaxError Syntax.SyntaxError
    | TypeError TypeCheck.Error

showErr :: Show a => a -> String
showErr x = "small-tt: error: " ++ show x

instance Show CompileError where
    show err = case err of
        PreError err -> showErr err
        SyntaxError err -> showErr err
        TypeError err -> showErr err

load :: String -> Bool -> IO (Either CompileError (Either Syntax.Tm (IO Int)))
load file run = do
    prep <- runExceptT (Preprocessor.preprocessor safeReadFile' file)
    case prep of
        Left err -> return (Left (PreError err))
        Right prep -> do
            let (files,pragmas) = prep
            seq files (return ())
            parse <- Syntax.loadFiles readFile' files
            case parse of
                Left err -> return (Left (SyntaxError err))
                Right loadtype -> do
                    let program = case loadtype of
                                    Syntax.Program tm -> tm
                                    Syntax.Library tm' -> tm' (Syntax.U (newPos file (-1) (-1)) (newPos file (-1) (-1)))
                    case TypeCheck.semantics (TypeCheck.SemanticConfig run pragmas) program of
                        Left err -> return (Left (TypeError err))
                        Right right -> return (Right right)

data CompilerConfig = CC { loadFile :: Maybe String
                         , printVersion :: Bool
                         , printNormalForm :: Bool
                         , runProgram :: Bool
                         , printHelp :: Bool }

initCC :: CompilerConfig
initCC = CC Nothing False False False False

data ParameterError = NoInputFile
                    | UnknownParameter String
                    | MoreThanOneFile

instance Show ParameterError where
    show pe = case pe of
        NoInputFile -> "no input file specified"
        UnknownParameter s -> "unknown parameter '" ++ s ++ "'"
        MoreThanOneFile -> "more than one input file specified"

processArgs :: [String] -> Either ParameterError CompilerConfig
processArgs args = do
    filename <- getFilename args
    let cc = initCC { loadFile = filename}
    cc <- getCommands args cc [ hasCommand "version" "" (\cc -> cc { printVersion = True })
                              , hasCommand "normalform" "nf" (\cc -> cc { printNormalForm = True })
                              , hasCommand "run" "r"  (\cc -> cc { runProgram = True })
                              , hasCommand "help" ""  (\cc -> cc { printHelp = True }) ]
    checkCC cc
    return cc
    where
        isCommand :: String -> Bool
        isCommand ('-':_) = True
        isCommand _ = False
        hasCommand :: String -> String -> (CompilerConfig -> CompilerConfig) -> (Set String,CompilerConfig) -> (Set String,CompilerConfig)
        hasCommand name short update (args,cc) =
            if Set.member ("--"++name) args || Set.member ('-':short) args then
                (Set.delete ("--"++name) . Set.delete ('-':short) $ args, update cc)
            else
                (args,cc)
        getCommands args cc xs =
            case foldl (\x f -> f x) (Set.fromList . filter isCommand $ args,cc) xs of
                (rest,cc) ->        
                    if Set.null rest then
                        Right cc
                    else
                        Left . UnknownParameter . head . Set.toList $ rest
        getFilename :: [String] -> Either ParameterError (Maybe String)
        getFilename [] = Right Nothing
        getFilename (x:xs) | isCommand x = getFilename xs
        getFilename (x:xs) = case getFilename xs of
                                Right Nothing -> Right . Just $ x
                                _ -> Left MoreThanOneFile                      
        checkCC :: CompilerConfig -> Either ParameterError ()
        checkCC (CC file _ nf run _) | nf || run = maybe (Left NoInputFile) (\_ -> Right ()) file
        checkCC _ = return ()

run :: String -> Bool -> Bool -> IO ()
run file runConf nf = do
    result <- load file runConf
    case result of
        Left error -> do
            print error
            exitFailure
        Right loaded -> case loaded of
            Left tm -> do
                putStrLn $ "Loaded " ++ file
                when nf (print tm)
                exitSuccess
            Right program -> do
                when nf (putStrLn "warning: -nf is overridden by --run")
                code <- program
                case code of
                    0 -> exitSuccess
                    _ -> exitWith (ExitFailure code)

main :: IO ()
main = do
    args <- getArgs
    case processArgs args of
        Left err -> do
            putStrLn . showErr $ err
            exitFailure
        Right config -> do
            when (printVersion config) version
            when (printHelp config)
                (putStrLn PrettySyntax.help)
            case loadFile config of
                Nothing -> exitSuccess
                Just filename -> run filename (runProgram config) (printNormalForm config)
    where version = putStrLn "SmallTT interpreter, version 0.1.0.0"
