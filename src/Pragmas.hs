module Pragmas(Pragma(..),parsePragma) where

data Pragma
    = Program
    | RunProgram
    deriving(Eq)

instance Show Pragma where
    show pragma = case pragma of
                    Program -> "program"                        
                    RunProgram -> "runprogram"

parsePragma :: String -> Maybe Pragma
parsePragma name = lookup name knownPragmas
                   where
                       knownPragmas :: [(String,Pragma)]
                       knownPragmas = [ ("program",Program)
                                      , ("runprogram",RunProgram)
                                      ]
