module Main where

import qualified Compiler
{-
main = do
    args <- getArgs
    mapM compileFile  args
    where
        compileFile :: String -> IO ()
        compileFile fileName = do
            putStrLn fileName
            source <- readFile fileName
            print . Compiler.exec fileName $ source
            putStrLn ""
-}

main = Compiler.main
